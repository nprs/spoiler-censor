<?php
/**
 * @package Spoiler_Censor
 * @version 0.1
 */
/*
Plugin Name: Spoiler Censor
Plugin URI: http://wordpress.org/plugins/spoiler-censor/
Description: Censor the spoilers!
Author: NPRS 
Version: 0.1
Author URI: http://pusparaaz.com.np/
*/

function censor($content) {
        $content = preg_replace("/<spoiler>/",
                "<span class=spoilercensor><span class=spoilermessage>Spoiler here!!!</span><span class=spoilercontent>",$content);
    $content = preg_replace("/<\/spoiler>/","</span></span>",$content);
    return $content;
}

// Now we set that function up to execute when the admin_notices action is called
add_filter( 'the_content', 'censor' );
add_filter( 'comment_text', 'censor' );

// We need some CSS to style the spoiler
function spoiler_css() {
    echo "<style type='text/css'>
    .spoilercensor{
        padding: 5px;
        color: red !important;
        background-color: black;
        text-align: center;
    }
    .spoilercontent {
        display: none;
    }
    .spoilercensor:hover > .spoilermessage {
        display: none;
    }
    .spoilercensor:hover > .spoilercontent {
        display: inline;
        color: white !important;
    }
</style>
    ";
}

add_action('wp_head','spoiler_css' );

?>
